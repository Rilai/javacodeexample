package com.keyintegrity.shb.startup.deal;

import com.keyintegrity.shb.common.id.ClientLink;
import com.keyintegrity.shb.common.id.CodeCatalogId;
import com.keyintegrity.shb.common.id.DealApplicationId;
import com.keyintegrity.shb.common.id.DealId;
import com.keyintegrity.shb.common.id.IClientLink;
import com.keyintegrity.shb.common.id.ProductId;
import com.keyintegrity.shb.company.query.dto.catalog.PriorityTypeCatalogDto;
import com.keyintegrity.shb.dao.db.model.tables.records.DealApplHistoryRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.DealApplicationsRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.DealsRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.HistoryActionRequestsRecord;
import com.keyintegrity.shb.deal.query.dto.DealApplicationHistoryItemType;
import com.keyintegrity.shb.deal.query.dto.catalog.DealApplicationStatusTypeCatalogDto;
import com.keyintegrity.shb.deal.query.dto.catalog.DealStatusTypeCatalogDto;
import org.jooq.DSLContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.keyintegrity.shb.dao.db.model.Tables.DEALS;
import static com.keyintegrity.shb.dao.db.model.Tables.DEAL_APPLICATIONS;
import static com.keyintegrity.shb.dao.db.model.Tables.DEAL_APPL_HISTORY;
import static com.keyintegrity.shb.dao.db.model.Tables.HISTORY_ACTION_REQUESTS;
import static com.keyintegrity.shb.deal.query.dto.catalog.DealAppHistoryActionRequestCatalogDto.REQUEST_CANCEL_BY_CLIENT;
import static com.keyintegrity.shb.deal.query.dto.catalog.DealAppHistoryActionRequestCatalogDto.SEND;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.action;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.dealApplicationId;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.dealId;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.name;
import static com.keyintegrity.shb.teststub.utils.IdConverterUtil.identifierIdToLong;
import static com.keyintegrity.shb.teststub.utils.IdConverterUtil.uuidToLong;

public class DealBuilder {
    interface CONTEXT<T> extends Function<DSLContext, Function<SomeGenerator, T>> {
    }

    private final Function<SomeGenerator, ClientLink> client;
    private final List<CONTEXT<DealApplicationsRecord>> dealApplications = new ArrayList<>();
    private final List<CONTEXT<List<LocalDealApplHistory>>> localDealApplHistories = new ArrayList<>();
    private final String dealName;

    public DealBuilder(String dealName, Function<SomeGenerator, ClientLink> client) {
        this.dealName = dealName;
        this.client = client;
    }

    public static DealBuilder aDeal(String dealName, Function<SomeGenerator, ClientLink> client) {
        return new DealBuilder(dealName, client);
    }

    public static LocalDateTime createdAt(String time) {
        return LocalDateTime.parse(time);
    }

    public DealBuilder withDealApplication(String dealApplName,
                                           Function<SomeGenerator, ProductId> product,
                                           LocalDateTime createdAt,
                                           CONTEXT<Function<String, List<LocalDealApplHistory>>> history) {
        dealApplications.add(dealApplicationBuilder(dealApplName, product, createdAt));
        localDealApplHistories.add(dealApplicationHistoryBuilder(dealApplName, history));
        return this;
    }

    public Store initWith(SomeGenerator ctx) {
        return dslContext -> {
            dealBuilder(client).apply(dslContext).apply(ctx).store();
            dealApplications.forEach(cons -> cons.apply(dslContext).apply(ctx).store());
            localDealApplHistories.stream()
                    .flatMap(cons -> cons.apply(dslContext)
                            .apply(ctx).stream())
                    .forEach(LocalDealApplHistory::store);
        };
    }

    interface Store {
        void to(DSLContext dslContext);
    }

    private CONTEXT<List<LocalDealApplHistory>> dealApplicationHistoryBuilder(
            String dealApplName,
            CONTEXT<Function<String, List<LocalDealApplHistory>>> history) {
        return dslContext -> ctx -> history.apply(dslContext).apply(ctx).apply(dealApplName);
    }

    private CONTEXT<DealApplicationsRecord> dealApplicationBuilder(String dealApplName,
                                                                   Function<SomeGenerator, ProductId> product,
                                                                   LocalDateTime createdAt) {
        return dslContext -> ctx -> {
            DealApplicationId dealApplicationId = ctx.some(dealApplicationId(dealApplName));
            return dslContext.newRecord(DEAL_APPLICATIONS)
                    .setId(identifierIdToLong(dealApplicationId))
                    .setExternalid(dealApplicationId.getRawId())
                    .setDealid(identifierIdToLong(ctx.some(dealId(dealName))))
                    .setStatusid(DealApplicationStatusTypeCatalogDto.CREATED.getId())
                    .setProductid(identifierIdToLong(product.apply(ctx)))
                    .setCreatedt(createdAt);
        };
    }

    private CONTEXT<DealsRecord> dealBuilder(Function<SomeGenerator, ClientLink> client) {
        return dslContext -> ctx -> {
            DealId dealId = ctx.some(dealId(dealName));
            IClientLink clientLink = ctx.some(client);
            return dslContext.newRecord(DEALS)
                    .setId(identifierIdToLong(dealId))
                    .setExternalid(dealId.getRawId())
                    .setDealNumber("1")
                    .setName(ctx.some(name(dealName)))
                    .setCurrencyType(1L)
                    .setProductType("BG")
                    .setPriority(PriorityTypeCatalogDto.MEDIUM.getId())
                    .setBaseOnType("OTHER")
                    .setClient(identifierIdToLong(clientLink.getId()))
                    .setClientType(clientLink.getClientType().getId())
                    .setClientContactPhone("123456")
                    .setClientContactEmail("test@test.test")
                    .setStatusid(DealStatusTypeCatalogDto.CREATED.getId())
                    .setAuthor(1L)
                    .setCreatedt(LocalDateTime.now());
        };
    }

    @SafeVarargs
    public static CONTEXT<Function<String, List<LocalDealApplHistory>>> withHistory(CONTEXT<Function<String, LocalDealApplHistory>>... funcs) {
        return dslContext -> ctx -> appName ->
                Arrays.stream(funcs)
                        .map(func -> func.apply(dslContext).apply(ctx).apply(appName))
                        .collect(Collectors.toList());
    }

    public static CONTEXT<Function<String, LocalDealApplHistory>> send(String actionName, LocalDateTime createdAt) {
        return createDealApplHistory(actionName, createdAt).apply(SEND);
    }

    public static CONTEXT<Function<String, LocalDealApplHistory>> canceled(String actionName, LocalDateTime createdAt) {
        return createDealApplHistory(actionName, createdAt).apply(REQUEST_CANCEL_BY_CLIENT);
    }

    private static Function<CodeCatalogId, CONTEXT<Function<String, LocalDealApplHistory>>> createDealApplHistory(String actionName, LocalDateTime createdAt) {
        return action -> dslContext -> ctx -> dealApplName -> {
            UUID actionId = action(dealApplName + "_FOR_HIST_ID_" + actionName).apply(ctx);

            DealApplHistoryRecord dealApplHistory = dslContext.newRecord(DEAL_APPL_HISTORY)
                    .setId(uuidToLong(actionId))
                    .setDealapplicationid(identifierIdToLong(ctx.some(dealApplicationId(dealApplName))))
                    .setCreatedt(createdAt)
                    .setType(DealApplicationHistoryItemType.ACTION_REQUEST.name())
                    .setExternalid(actionId)
                    .setMsgid(actionId);

            HistoryActionRequestsRecord historyActionRequests = dslContext.newRecord(HISTORY_ACTION_REQUESTS)
                    .setId(dealApplHistory.getId())
                    .setActionreqtypeid(action.getId());

            return new LocalDealApplHistory(dealApplHistory, historyActionRequests);
        };
    }

    private static class LocalDealApplHistory {
        final DealApplHistoryRecord dealApplHistoryRecord;
        final HistoryActionRequestsRecord historyActionRequestsRecord;

        public LocalDealApplHistory(DealApplHistoryRecord dealApplHistoryRecord, HistoryActionRequestsRecord historyActionRequestsRecord) {

            this.dealApplHistoryRecord = dealApplHistoryRecord;
            this.historyActionRequestsRecord = historyActionRequestsRecord;
        }

        void store() {
            dealApplHistoryRecord.store();
            historyActionRequestsRecord.store();
        }
    }
}
