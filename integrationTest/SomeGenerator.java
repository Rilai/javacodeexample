package com.keyintegrity.shb.startup.deal;

import com.keyintegrity.shb.common.Identifier;
import com.keyintegrity.shb.common.id.*;
import lombok.Getter;

import java.util.UUID;
import java.util.function.Function;

public class SomeGenerator {
    @Getter
    private String impurity;

    private SomeGenerator(String impurity) {
        this.impurity = impurity;
    }
    public static SomeGenerator of(String impurity) {
        return new SomeGenerator(impurity);
    }

    static Function<SomeGenerator, DealApplicationId> dealApplicationId(String dealApplName) {
        return ctx -> new DealApplicationId(ctx.uuidFromName(dealApplName));
    }

    static Function<SomeGenerator, ProductId> productId(String productName, String serviceName) {
        return ctx -> new ProductId(ctx.uuidFromName(productName), ctx.some(serviceId(serviceName)));
    }

    static Function<SomeGenerator, DealId> dealId(String dealName) {
        return ctx -> new DealId(ctx.uuidFromName(dealName));
    }

    static Function<SomeGenerator, ClientLink> clientCompany(String clientName) {
        return ctx -> ClientLink.createCompanyLink(ctx.some(companyId(clientName)));
    }

    static Function<SomeGenerator, CompanyId> companyId(String clientName) {
        return ctx -> new CompanyId(ctx.uuidFromName(clientName));
    }

    static Function<SomeGenerator, ServiceId> serviceId(String name) {
        return ctx -> new ServiceId(ctx.uuidFromName(name));
    }
    static Function<SomeGenerator, String> name(String name) {
        return ctx -> name+"_"+ ctx.impurity;
    }

    static Function<SomeGenerator, UUID> action(String name) {
        return ctx -> ctx.uuidFromName(name);
    }

    <T> T some(Function<SomeGenerator, T> func) {
        return func.apply(this);
    }

    private UUID uuidFromName(String name) {
        return UUID.nameUUIDFromBytes((name + impurity).getBytes());
    }

}
