package com.keyintegrity.shb.startup.deal;

import com.keyintegrity.shb.common.IdRegistry;
import com.keyintegrity.shb.common.id.ClientLinkRdbms;
import com.keyintegrity.shb.common.id.CodeCatalogId;
import com.keyintegrity.shb.common.id.CompanyId;
import com.keyintegrity.shb.common.id.CompanyRdbmsId;
import com.keyintegrity.shb.common.id.ProductId;
import com.keyintegrity.shb.common.id.ServiceId;
import com.keyintegrity.shb.common.id.ServiceRdbmsId;
import com.keyintegrity.shb.company.query.dto.catalog.PriorityTypeCatalogDto;
import com.keyintegrity.shb.dao.DealDao;
import com.keyintegrity.shb.dao.db.model.tables.records.PrincipalsRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.ServicesRecord;
import com.keyintegrity.shb.service.query.dto.catalog.ProductTypeCatalogDto;
import com.keyintegrity.shb.startup.AbstractOAuth2IntegrationTest;
import com.keyintegrity.shb.user.query.dto.catalog.ParticipantTypeCatalogDto;
import com.keyintegrity.shb.user.query.dto.catalog.PrincipalTypeCatalogDto;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.jooq.DSLContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static com.keyintegrity.shb.dao.db.model.Tables.COMPANIES;
import static com.keyintegrity.shb.dao.db.model.Tables.PARTICIPANTS;
import static com.keyintegrity.shb.dao.db.model.Tables.PRINCIPALS;
import static com.keyintegrity.shb.dao.db.model.Tables.PRODUCTS;
import static com.keyintegrity.shb.dao.db.model.Tables.SERVICES;
import static com.keyintegrity.shb.startup.deal.DealBuilder.aDeal;
import static com.keyintegrity.shb.startup.deal.DealBuilder.canceled;
import static com.keyintegrity.shb.startup.deal.DealBuilder.createdAt;
import static com.keyintegrity.shb.startup.deal.DealBuilder.send;
import static com.keyintegrity.shb.startup.deal.DealBuilder.withHistory;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.clientCompany;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.companyId;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.name;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.productId;
import static com.keyintegrity.shb.startup.deal.SomeGenerator.serviceId;
import static com.keyintegrity.shb.teststub.utils.IdConverterUtil.identifierIdToLong;
import static org.assertj.core.api.Assertions.assertThat;

public class DealDaoTest extends AbstractOAuth2IntegrationTest {
    @Autowired
    private DSLContext dslContext;
    @Autowired
    private DealDao dealDao;
    @Autowired
    private IdRegistry idRegistry;

    @Test
    void should_return_last_sended_to_service_deal_application_create_date() {
        idRegistry.init();
        SomeGenerator ctx = SomeGenerator.of(RandomString.make(5));

        initService("SBER", ctx)
                .withProduct("BG", ProductTypeCatalogDto.BG);

        initParticipant("CLIENT", ctx);
        initClientCompany("CLIENT", ctx);

        aDeal("BG1", clientCompany("CLIENT"))
                .withDealApplication("BG_IN_SBER1",
                        productId("BG", "SBER"),
                        createdAt("2021-05-01T12:00:00"),
                        withHistory(send("ACTION_1", createdAt("2021-05-02T12:00:00")),
                                canceled("ACTION_2", createdAt("2021-05-03T12:00:00"))))
                .initWith(ctx).to(dslContext);

        aDeal("BG2", clientCompany("CLIENT"))
                .withDealApplication("BG_IN_SBER2",
                        productId("BG", "SBER"),
                        createdAt("2021-07-22T12:00:00"),
                        withHistory(send("ACTION_1", createdAt("2021-07-23T12:00:00")),
                                canceled("ACTION_2", createdAt("2021-07-24T12:00:00"))))
                .initWith(ctx).to(dslContext);

        aDeal("BG3", clientCompany("CLIENT"))
                .withDealApplication("BG_IN_SBER3",
                        productId("BG", "SBER"),
                        createdAt("2022-01-01T12:00:00"),
                        withHistory(canceled("ACTION_1", createdAt("2022-01-02T12:00:00"))))
                .initWith(ctx).to(dslContext);

        ServiceRdbmsId serviceRdbmsId = new ServiceRdbmsId(identifierIdToLong(ctx.some(serviceId("SBER"))));
        ClientLinkRdbms clientLinkRdbms = ClientLinkRdbms.createCompanyLink(new CompanyRdbmsId(identifierIdToLong(ctx.some(companyId("CLIENT")))));

        assertThat(dealDao.getPreviousApplicationDate(serviceRdbmsId, clientLinkRdbms))
                .isEqualTo(LocalDateTime.parse("2021-07-23T12:00:00"));

    }

    private void initParticipant(String clientName, SomeGenerator ctx) {
        CompanyId client = ctx.some(companyId(clientName));
        Long id = identifierIdToLong(client);

        dslContext.newRecord(PARTICIPANTS)
                .setId(id)
                .setExternalid(client.getRawId())
                .setTypeid(ParticipantTypeCatalogDto.COMPANY_CONTACT.getId())
                .store();
    }

    private void initClientCompany(String clientName, SomeGenerator ctx) {
        CompanyId client = ctx.some(companyId(clientName));
        Long id = identifierIdToLong(client);

        dslContext.newRecord(COMPANIES)
                .setId(id)
                .setExternalId(client.getRawId())
                .setAuthor(1L)
                .setCreateDateTime(LocalDateTime.now())
                .store();
    }

    private ProductsBuilder initService(String serviceName, SomeGenerator ctx) {
        ServiceId serviceId = ctx.some(serviceId(serviceName));
        Long serviceLongId = identifierIdToLong(serviceId);

        PrincipalsRecord principalsRecord = dslContext.newRecord(PRINCIPALS)
                .setId(serviceLongId)
                .setExternalid(serviceId.getRawId())
                .setTypeid(PrincipalTypeCatalogDto.SERVICE.getId());

        ServicesRecord servicesRecord = dslContext.newRecord(SERVICES)
                .setId(serviceLongId)
                .setSysval(ctx.some(name(serviceName)))
                .setName(ctx.some(name(serviceName)))
                .setShortname(ctx.some(name(serviceName)))
                .setAuthor(1L)
                .setCreatedt(LocalDateTime.now())
                .setPriorityType(PriorityTypeCatalogDto.MEDIUM.getId())
                .setState("1");

        principalsRecord.store();
        servicesRecord.store();

        return new ProductsBuilder(servicesRecord, ctx);
    }

    private class ProductsBuilder {

        final ServicesRecord servicesRecord;
        final SomeGenerator ctx;

        ProductsBuilder(ServicesRecord servicesRecord, SomeGenerator ctx) {
            this.servicesRecord = servicesRecord;
            this.ctx = ctx;
        }

        ProductsBuilder withProduct(String productName, CodeCatalogId productType) {

            ProductId productId = ctx.some(productId(productName, servicesRecord.getName()));

            dslContext.newRecord(PRODUCTS)
                    .setId(identifierIdToLong(productId))
                    .setExternalid(productId.getRawId())
                    .setServiceid(servicesRecord.getId())
                    .setName(productName)
                    .setShortname(productName)
                    .setAuthor(1L)
                    .setProducttypeid(productType.getId())
                    .setState("1")
                    .setCreatedt(LocalDateTime.now())
                    .store();
            return this;
        }

    }

}



















