package com.keyintegrity.shb.deal.handler;

import com.keyintegrity.shb.clientsupportqueue.DealApplicationSysStatusChangedEvent;
import com.keyintegrity.shb.common.AssertUtils;
import com.keyintegrity.shb.common.TestMessageBuilder;
import com.keyintegrity.shb.common.id.CodeCatalogId;
import com.keyintegrity.shb.common.id.DealApplicationId;
import com.keyintegrity.shb.common.id.DealId;
import com.keyintegrity.shb.common.id.EntityLink;
import com.keyintegrity.shb.common.id.ProductId;
import com.keyintegrity.shb.common.id.ServiceId;
import com.keyintegrity.shb.dao.db.model.tables.records.DealApplHistoryRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.DealApplicationsRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.DealsRecord;
import com.keyintegrity.shb.dao.db.model.tables.records.HistoryStatusChangeRecord;
import com.keyintegrity.shb.deal.CancelDealApplications;
import com.keyintegrity.shb.deal.DealServiceResponseStatusInfo;
import com.keyintegrity.shb.deal.TestDealApplicationContext;
import com.keyintegrity.shb.deal.mq.CancelDealApplication;
import com.keyintegrity.shb.deal.query.dto.DealApplicationHistoryItemType;
import com.keyintegrity.shb.deal.query.dto.catalog.DealApplicationStatusTypeCatalogDto;
import com.keyintegrity.shb.deal.query.dto.catalog.DealStatusTypeCatalogDto;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.keyintegrity.shb.common.AssertUtils.assertThat;
import static com.keyintegrity.shb.common.AssertUtils.assertThatNotice;
import static com.keyintegrity.shb.teststub.company.TestCompanyInfoGenerator.someAgentCompany;
import static com.keyintegrity.shb.teststub.company.TestCompanyInfoGenerator.someClientLink;
import static com.keyintegrity.shb.teststub.deal.TestDealInfoGenerator.someDealApplication;
import static com.keyintegrity.shb.teststub.deal.TestDealInfoGenerator.someDealId;
import static com.keyintegrity.shb.teststub.deal.TestDealInfoGenerator.someProductId;
import static com.keyintegrity.shb.teststub.deal.TestDealInfoGenerator.someServiceId;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.assertThat;

class ChangeDealApplicationStatusCommandHandlerTest {

    TestDealApplicationContext<ChangeDealApplicationStatusCommandHandler> ctx;
    LocalDateTime now = LocalDate.parse("2008-10-10").atStartOfDay();

    @BeforeEach
    void setUp() {
        ctx = TestDealApplicationContext.of(ChangeDealApplicationStatusCommandHandler.class, now,
                CancelDealApplications.class);


        DealId DEAL_BG = someDealId("BG");
        DealApplicationId DEAL_APPL_BG_IN_SBER = someDealApplication("BG_IN_SBER");
        DealApplicationId DEAL_APPL_BG_IN_VTB = someDealApplication("BG_IN_VTB");
        ProductId BG_IN_SBER = someProductId("BG", "SBER");
        ProductId BG_IN_VTB = someProductId("BG", "VTB");

        ctx.dealRepository.initDeal(DEAL_BG, DealStatusTypeCatalogDto.CREATED)
                .withClientAgent(someClientLink("CLIENT"),
                        someAgentCompany("AGENT"))
                .withApplication(DEAL_APPL_BG_IN_SBER, BG_IN_SBER, DealApplicationStatusTypeCatalogDto.SENT_TO_SERVICE)
                .withApplication(DEAL_APPL_BG_IN_VTB, BG_IN_VTB, DealApplicationStatusTypeCatalogDto.SENT_TO_SERVICE);
        ctx.dealDao.initDeal(DEAL_BG);


    }

    @Test
    void should_give_error_if_deal_application_not_found() {
        ThrowableAssert.ThrowingCallable callable = TestMessageBuilder.sendLoggedInMessage(ChangeDealApplicationStatusCommand
                        .builder(someDealApplication("ANOTHER"), "bank_refusal")
                        .build())
                .asSysAdmin()
                .toAsCallable(ctx.sut::handle);

        assertThatThrownBy(callable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("dealApplicationsRecord not found");
    }

    @Test
    void should_give_error_if_deal_application_history_not_found() {
        ThrowableAssert.ThrowingCallable callable = TestMessageBuilder.sendLoggedInMessage(ChangeDealApplicationStatusCommand
                        .builder(someDealApplication("BG_IN_SBER"), "bank_refusal").build())
                .asSysAdmin()
                .toAsCallable(ctx.sut::handle);

        assertThatThrownBy(callable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("deal application history not found");
    }

    @Test
    void should_create_deal_appl_history() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initStatusToService(SBER,
                "bank_refusal",
                DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE,
                "Отклонена банком");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "bank_refusal")
                .withMsgId(msgId).build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);

        assertThat(ctx.dealRepository.getAllDealApplHistoryFor(BG_IS_SBER))
                .filteredOn(rec -> msgId.equals(rec.getMsgid()))
                .first(AssertUtils.record(DealApplHistoryRecord.class))
                .isEqualWithExtracting(DealApplHistoryRecord::getDealapplicationid, BG_IS_SBER)
                .isEqualWithExtracting(DealApplHistoryRecord::getType, DealApplicationHistoryItemType.STATUS_CHANGE.name())
                .isEqualWithExtracting(DealApplHistoryRecord::getCreatedt, now);
    }

    @Test
    void should_create_history_status_change() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initStatusToService(SBER,
                "bank_refusal",
                DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE,
                "Отклонена банком");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "bank_refusal")
                .withMsgId(msgId)
                .withMessage("TEST_MESSAGE").build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);
        DealApplHistoryRecord dealApplHistoryRecord = ctx.dealRepository.getAllDealApplHistoryFor(BG_IS_SBER).stream()
                .filter(rec -> msgId.equals(rec.getMsgid()))
                .findFirst().orElseThrow();
        assertThat(ctx.dealRepository.getHistoryStatusChange(dealApplHistoryRecord))
                .isEqualWithExtracting(HistoryStatusChangeRecord::getSysstatus, "bank_refusal")
                .isEqualWithExtracting(HistoryStatusChangeRecord::getMessage, "TEST_MESSAGE");
    }

    @Test
    void should_change_deal_application_status() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initStatusToService(SBER,
                "bank_refusal",
                DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE,
                "Отклонена банком");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "bank_refusal")
                .withMsgId(msgId)
                .withMessage("TEST_MESSAGE").build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);

        assertThat(ctx.dealRepository.getDealApplicationById(BG_IS_SBER))
                .isEqualWithExtracting(DealApplicationsRecord::getStatusid, DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE)
                .isEqualWithExtracting(DealApplicationsRecord::getModifieddt, now);
    }

    @Test
    void should_dont_change_deal_application_status_to_intermediate_status() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initIntermediateStatusToService(SBER,
                "payment_and_issue",
                "Оплата и выпуск");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "payment_and_issue")
                .withMsgId(msgId)
                .withMessage("TEST_MESSAGE").build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);

        assertThat(ctx.dealRepository.getDealApplicationById(BG_IS_SBER))
                .isEqualWithExtracting(DealApplicationsRecord::getStatusid, DealApplicationStatusTypeCatalogDto.SENT_TO_SERVICE);
    }

    @Nested
    class product_issued {
        @BeforeEach
        void setUp() {
            DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
            ServiceId SBER = someServiceId("SBER");

            ctx.dealRepository.setDealApplicationStatus(BG_IS_SBER, DealApplicationStatusTypeCatalogDto.SENT_TO_SERVICE);

            ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
            ctx.serviceRepository.initServiceRecords(SBER);

            initStatusToService(SBER,
                    "issued",
                    DealApplicationStatusTypeCatalogDto.PRODUCT_ISSUED,
                    "Выпущен продукт");

            UUID msgId = UUID.randomUUID();
            ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                    .builder(BG_IS_SBER, "issued")
                    .withMsgId(msgId)
                    .withMessage("TEST_MESSAGE").build();

            TestMessageBuilder.sendLoggedInMessage(command)
                    .asSysAdmin()
                    .to(ctx.sut::handle);
        }

        @Test
        void should_cancel_other_deal_applications() {
            assertThat(ctx.serviceIntegrationTemplateHolder.getAllMessages())
                    .filteredOn(queueMessage -> queueMessage.getPayloadType().equals(CancelDealApplication.class.getSimpleName()))
                    .extracting(queueMessage -> (CancelDealApplication) queueMessage.getPayload())
                    .extracting(CancelDealApplication::getDealApplicationIds)
                    .extracting(ids -> ids.get(0))
                    .containsOnly(someDealApplication("BG_IN_VTB"));
        }

        @Test
        void should_change_deal_status_to_SUCCESS() {
            assertThat(ctx.dealRepository.getDealById(someDealId("BG")))
                    .isEqualWithExtracting(DealsRecord::getStatusid, DealStatusTypeCatalogDto.SUCCESS);
        }
    }

    @Test
    void should_send_deal_appl_sys_status_change_event() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initStatusToService(SBER,
                "bank_refusal",
                DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE,
                "Отклонена банком");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "bank_refusal")
                .withMsgId(msgId)
                .withMessage("TEST_MESSAGE").build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);

        assertThat(ctx.eventBus.getEventByPayLoadType(DealApplicationSysStatusChangedEvent.class))
                .isPresent().get()
                .satisfies(event -> {
                    assertThat(event.getEntityLink())
                            .isEqualTo(EntityLink.createFromDealApplicationId(BG_IS_SBER));
                    assertThat(event.getDealApplicationStatus())
                            .isEqualTo(DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE);
                    assertThat(event.getSysStatus()) //new status
                            .isEqualTo("bank_refusal");
                    assertThat(event.getDisplayStatus())
                            .isEqualTo("Отклонена банком");
                    assertThat(event.getStatusDateTime())
                            .isEqualTo(now);
                });
    }

    @Test
    void should_send_service_status_change_notice() {
        DealApplicationId BG_IS_SBER = someDealApplication("BG_IN_SBER");
        ServiceId SBER = someServiceId("SBER");

        ctx.dealRepository.initDealApplHistory(BG_IS_SBER);
        ctx.serviceRepository.initServiceRecords(SBER);

        initStatusToService(SBER,
                "bank_refusal",
                DealApplicationStatusTypeCatalogDto.REJECTED_BY_SERVICE,
                "Отклонена банком");

        UUID msgId = UUID.randomUUID();
        ChangeDealApplicationStatusCommand command = ChangeDealApplicationStatusCommand
                .builder(BG_IS_SBER, "bank_refusal")
                .withMsgId(msgId)
                .withMessage("TEST_MESSAGE").build();

        TestMessageBuilder.sendLoggedInMessage(command)
                .asSysAdmin()
                .to(ctx.sut::handle);

        assertThatNotice(ctx.noticeManager.getNotices())
                .isEntityContain(someDealId("BG"));
    }

    private void initStatusToService(ServiceId SBER, String serviceStatus, CodeCatalogId status, String displayStatus) {
        DealServiceResponseStatusInfo dealStatusInfo = new DealServiceResponseStatusInfo(status, displayStatus, null);
        ctx.serviceDao.initStatusMapping(SBER, serviceStatus, dealStatusInfo);
        ctx.serviceConfig.afterPropertiesSet();
    }
    private void initIntermediateStatusToService(ServiceId SBER, String serviceStatus, String displayStatus) {
        DealServiceResponseStatusInfo dealStatusInfo = new DealServiceResponseStatusInfo(null, displayStatus, null);
        ctx.serviceDao.initStatusMapping(SBER, serviceStatus, dealStatusInfo);
        ctx.serviceConfig.afterPropertiesSet();
    }
}