package com.keyintegrity.shb.deal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.keyintegrity.shb.TestApplicationContextBuilder;
import com.keyintegrity.shb.common.ServiceIntegrationTemplateHolderStub;
import com.keyintegrity.shb.common.query.QueryResolverImpl;
import com.keyintegrity.shb.service.ServiceConfig;
import com.keyintegrity.shb.teststub.common.CommonDaoStub;
import com.keyintegrity.shb.teststub.common.IdConverterConfigStub;
import com.keyintegrity.shb.teststub.common.IdFactoryStub;
import com.keyintegrity.shb.teststub.common.ShbUiUrlProviderStub;
import com.keyintegrity.shb.teststub.common.converter.IdConverterStub;
import com.keyintegrity.shb.teststub.company.CompanyRepositoryStub;
import com.keyintegrity.shb.teststub.dao.ServiceDaoStub;
import com.keyintegrity.shb.teststub.deal.DealDaoStub;
import com.keyintegrity.shb.teststub.deal.DealRepositoryStub;
import com.keyintegrity.shb.teststub.eds.SignatureHelperStub;
import com.keyintegrity.shb.teststub.eventbus.EventBusStub;
import com.keyintegrity.shb.teststub.notice.NoticeManagerStub;
import com.keyintegrity.shb.teststub.product.ProductRepositoryStub;
import com.keyintegrity.shb.teststub.service.ServiceRepositoryStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;


@SuppressWarnings("unchecked")
public class TestDealApplicationContext<SUT> {

    @Qualifier("sut")
    @Autowired public SUT sut;
    @Autowired public DealRepositoryStub dealRepository;
    @Autowired public ServiceIntegrationTemplateHolderStub serviceIntegrationTemplateHolder;
    @Autowired public QueryResolverImpl queryResolver;
    @Autowired public ProductRepositoryStub productRepository;
    @Autowired public ServiceRepositoryStub serviceRepository;
    @Autowired public ServiceDaoStub serviceDao;
    @Autowired public ServiceConfig serviceConfig;
    @Autowired public IdConverterStub idConverter;
    @Autowired public EventBusStub eventBus;
    @Autowired public NoticeManagerStub noticeManager;
    @Autowired public DealDaoStub dealDao;
    @Autowired public CompanyRepositoryStub companyRepository;
    @Autowired public SignatureHelperStub signatureHelper;

    public static <SUT> TestDealApplicationContext<SUT> of(Class<SUT> sutClass, Class<?>... extraDeps) {
        AnnotationConfigApplicationContext context =
                TestApplicationContextBuilder.createContextWithTime(TestDealApplicationContext.class,
                        LocalDate.parse("2009-10-25").atStartOfDay(),
                        sutClass,
                        TestDealConfiguration.class,
                        extraDeps);
        return context.getBean(TestDealApplicationContext.class);
    }

    public static <SUT> TestDealApplicationContext<SUT> of(Class<SUT> sutClass, LocalDateTime dateTime, Class<?>... extraDeps) {
        AnnotationConfigApplicationContext context =
                TestApplicationContextBuilder.createContextWithTime(TestDealApplicationContext.class,
                        dateTime,
                        sutClass,
                        TestDealConfiguration.class,
                        extraDeps);
        return context.getBean(TestDealApplicationContext.class);
    }
    @Import(value = {
            DealRepositoryStub.class,
            DealDaoStub.class,
            ServiceRepositoryStub.class,
            ServiceIntegrationTemplateHolderStub.class,
            ProductRepositoryStub.class,
            QueryResolverImpl.class,
            ObjectMapper.class,
            ServiceDaoStub.class,
            CommonDaoStub.class,
            NoticeManagerStub.class,
            ShbUiUrlProviderStub.class,
            IdFactoryStub.class,
            IdConverterStub.class,
            IdConverterConfigStub.class,
            CompanyRepositoryStub.class,
            SignatureHelperStub.class
    })
    private static class TestDealConfiguration {
        @Bean
        public Clock clock(@Value("${now}") String dateTime) {
            return fixedClock(LocalDateTime.parse(dateTime));
        }
        @Bean
        @Qualifier("eventBus")
        public EventBusStub eventBus() {
            return new EventBusStub();
        }
        @Bean
        public ServiceConfig serviceConfig() {
            return new ServiceConfig();
        }
    }
    private static Clock fixedClock(LocalDateTime time) {
        ZoneId zone = ZoneId.systemDefault();
        return Clock.fixed(time.atZone(zone).toInstant(), zone);
    }

}
